#!/bin/bash

# System

sudo apt update
sudo DEBIAN_FRONTEND=noninteractive apt upgrade -yq
sudo DEBIAN_FRONTEND=noninteractive apt install -yq git htop net-tools unzip fail2ban

# Docker
sudo apt install -y docker.io
sudo systemctl enable docker
sudo systemctl start docker

# Python
sudo apt install -y python3-pip python3-venv

# HashiCorp Tools
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt install -y terraform packer

# Golang
cd /tmp
curl https://dl.google.com/go/go1.17.linux-amd64.tar.gz -o "go1.17.linux-amd64.tar.gz"
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go1.17.linux-amd64.tar.gz
rm go1.17.linux-amd64.tar.gz
echo 'export PATH=$PATH:/usr/local/go/bin:/usr/local/bin' >> /home/ubuntu/.bashrc

# AWS CLI
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
rm -rf aws*

# Sync script
cd /home/ubuntu
mkdir git
chown ubuntu:ubuntu git
cd git

cat <<EOF > sync
#!/bin/bash

git clone git@gitlab.com:devops-uncovered-book/book-content-mkdocs.git
git clone git@gitlab.com:ht9/application/httpcats.git book-application-golang
git clone git@gitlab.com:ht9/configuration/ansible.git book-configuration-ansible
git clone git@gitlab.com:ht9/infrastructure/terraform.git book-infrastructure-terraform
EOF

chown ubuntu:ubuntu sync
chmod +x sync
echo 'export CODE_HOME=/home/ubuntu/git' >> /home/ubuntu/.bashrc