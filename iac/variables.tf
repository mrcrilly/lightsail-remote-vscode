
variable "aws_region" {
    description = "The AWS region to build the Lightsail instance in"
    default = "ap-southeast-2"
    type = string
}

variable "aws_region_az" {
    description = "The Availability Zone (AZ) to set the instance up in"
    default = "a"
    type = string
}
