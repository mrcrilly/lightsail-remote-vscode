
provider "aws" {
    region = var.aws_region
}

resource "aws_lightsail_instance" "vscode" {
  name              = "vscode-dev-machine"
  blueprint_id      = "ubuntu_20_04"
  bundle_id         = "small_2_2"
  key_pair_name     = aws_lightsail_key_pair.vscode.id
  availability_zone = "${var.aws_region}${var.aws_region_az}"
  user_data = file("./files/provision.sh")

  tags = {
    Name = "VSCode Remote Development System"
  }
}

resource "aws_lightsail_key_pair" "vscode" {
  name       = "vscode"
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "aws_lightsail_static_ip" "vscode" {
  name = "vscode-development-system"
}

resource "aws_lightsail_static_ip_attachment" "vscode" {
  static_ip_name = aws_lightsail_static_ip.vscode.id
  instance_name  = aws_lightsail_instance.vscode.id
}
